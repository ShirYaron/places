package com.example.mapactivate;

import androidx.fragment.app.FragmentActivity;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{


    private static final String TAG = "shir";

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        //Log.d(TAG, "Null location returned.");
                        Context context = getApplicationContext();
                        CharSequence text = "Hello toast!";
                        int duration = Toast.LENGTH_LONG;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        if (location != null) {
                            LatLng place3 = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.addMarker(new MarkerOptions().position(place3).title("Marker in po"));
                            Context context1 = getApplicationContext();
                            CharSequence text1 = "yesss!";
                            int duration1 = Toast.LENGTH_LONG;

                            Toast toast1 = Toast.makeText(context1, text1, duration1);
                            toast.show();
                            //Log.d(TAG, "Location returned.");
                            // write code here to to make the API call to the weather service and update the UI
                            // this code here is running on the Main UI thread as far as I understand
                        } else {
                            Context context1 = getApplicationContext();
                            CharSequence text1 = "nooo!";
                            int duration1 = Toast.LENGTH_LONG;

                            Toast toast1 = Toast.makeText(context1, text1, duration1);
                            toast.show();

                        }
                    }
                });
        //Task<Location> l=mFusedLocationClient.getLastLocation();

        //LatLng place3 = new LatLng(lo.getLatitude(), lo.getLongitude());
        LatLng place1 = new LatLng(31.416259,  34.584211);
        //LatLng place2 = new LatLng(mLastLocation.getLongitude(),  mLastLocation.getLatitude());
        mMap.addMarker(new MarkerOptions().position(place1).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(place1));
    }


}

